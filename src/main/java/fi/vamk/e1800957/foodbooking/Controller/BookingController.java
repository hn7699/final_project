package fi.vamk.e1800957.foodbooking.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.vamk.e1800957.foodbooking.DTO.BookingDTO;
import fi.vamk.e1800957.foodbooking.DTO.ItemDTO;
import fi.vamk.e1800957.foodbooking.Entity.Booking;
import fi.vamk.e1800957.foodbooking.Entity.Item;
import fi.vamk.e1800957.foodbooking.Repository.BookingRepository;
import fi.vamk.e1800957.foodbooking.Repository.ItemRepository;


@RestController
public class BookingController {

	@Autowired
	private BookingRepository bookingRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	
	@GetMapping("/booking/{id}")
	public BookingDTO booking(@PathVariable("id") int id) {
		try {
			return bookingRepository.findById(id).get().convert();
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@GetMapping("/bookings")
	public Iterable<BookingDTO> list(){
		try {
			List<BookingDTO> list=new ArrayList<BookingDTO>();
			for (Booking b : bookingRepository.findAll()) {
				list.add(b.convert());
			}
			return list;
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@PostMapping("/booking")
	public @ResponseBody BookingDTO newBooking(@RequestBody BookingDTO booking ) {
		try {
			Booking b1= booking.convert();
			List<Item> list= new ArrayList<Item>();
			double price=0;
			for (ItemDTO itemDTO : booking.getItems()) {
				list.add(itemRepository.findById(itemDTO.getId()).get());
				price+=itemRepository.findById(itemDTO.getId()).get().getPrice();
			}
			b1.setPrice(price);
			b1.setItems(list);
			return bookingRepository.save(b1).convert();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@DeleteMapping("/booking/{id}")
	public void delete(@PathVariable("id") int id) {
		try {
			bookingRepository.deleteById(id);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@PutMapping("/booking")
	public @ResponseBody BookingDTO updateBooking(@RequestBody BookingDTO booking ) {
		try {
			Booking b1= booking.convert();
			b1.setId(booking.getId());
			List<Item> list= new ArrayList<Item>();
			double price=0;
			for (ItemDTO itemDTO : booking.getItems()) {
				list.add(itemRepository.findById(itemDTO.getId()).get());
				price+=itemRepository.findById(itemDTO.getId()).get().getPrice();
			}
			b1.setItems(list);
			b1.setPrice(price);
			return bookingRepository.save(b1).convert();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
