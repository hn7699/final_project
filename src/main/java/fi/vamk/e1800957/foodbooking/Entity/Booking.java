package fi.vamk.e1800957.foodbooking.Entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fi.vamk.e1800957.foodbooking.DTO.BookingDTO;
import fi.vamk.e1800957.foodbooking.DTO.ItemDTO;

@Entity
@Table(name="Booking")
//@NamedQuery(name = "Booking.findAll", query = "SELECT b FROM Booking b")
public class Booking implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String customer_name;
	private String customer_address;
	private String customer_phone;
	private String status;
	private Date time;
	private double price;
	
	//@OneToMany(mappedBy = "booking")
//	@JoinColumn(name = "item_id")
//	@OneToMany(
//	        cascade = CascadeType.ALL,
//	        orphanRemoval = true
//	    )
//	@JoinColumn(name = "item_id")
//	@OnDelete(action = OnDeleteAction.CASCADE)
	@ManyToMany()
	@JoinTable(name = "booking_item",
    joinColumns = @JoinColumn(name = "booking_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "item_id", referencedColumnName = "id"))
	private List<Item> items;
	
	

	public Booking() {
		super();
	}

	public Booking(String customer_name, String customer_address, String customer_phone, String status) {
		super();
		this.customer_name = customer_name;
		this.customer_address = customer_address;
		this.customer_phone = customer_phone;
		this.status = status;
		this.time=new Date();
	}
	

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_address() {
		return customer_address;
	}

	public void setCustomer_address(String customer_address) {
		this.customer_address = customer_address;
	}

	public String getCustomer_phone() {
		return customer_phone;
	}

	public void setCustomer_phone(String customer_phone) {
		this.customer_phone = customer_phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getPrice() {
		return price;
	}

	

	public void setPrice(double price) {
		this.price = price;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	public BookingDTO convert() {
		BookingDTO b1= new BookingDTO();
		b1.setId(this.id);
		b1.setCustomer_name(getCustomer_name());
		b1.setCustomer_address(getCustomer_address());
		b1.setCustomer_phone(getCustomer_phone());
		b1.setStatus(getStatus());
		b1.setPrice(getPrice());
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		b1.setTime(formatter.format(this.time));
		List<ItemDTO> list= new ArrayList<ItemDTO>();
		for (Item item : this.items) {
			list.add(item.convert());
		}
		b1.setItems(list);
		return b1;
	}

	@Override
	public String toString() {
		return "Booking [id=" + id + ", customer_name=" + customer_name + ", customer_address=" + customer_address
				+ ", customer_phone=" + customer_phone + ", status=" + status + ", time=" + time + ", price=" + price
				+ "]";
	}
	
	
	

}
