package fi.vamk.e1800957.foodbooking.Repository;

import org.springframework.data.repository.CrudRepository;

import fi.vamk.e1800957.foodbooking.Entity.Booking;


public interface BookingRepository extends CrudRepository<Booking, Integer>{

}
