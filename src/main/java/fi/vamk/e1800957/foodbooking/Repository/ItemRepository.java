package fi.vamk.e1800957.foodbooking.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fi.vamk.e1800957.foodbooking.Entity.Booking;
import fi.vamk.e1800957.foodbooking.Entity.Item;

public interface ItemRepository extends CrudRepository<Item, Integer>{
//	@Query(value="SELECT i FROM Item i WHERE i.bookings = ?1")
//	public List<Item> findByBooking(Booking booking);
}
