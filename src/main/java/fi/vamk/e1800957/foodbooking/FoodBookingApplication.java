package fi.vamk.e1800957.foodbooking;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fi.vamk.e1800957.foodbooking.DTO.BookingDTO;
import fi.vamk.e1800957.foodbooking.DTO.ItemDTO;
import fi.vamk.e1800957.foodbooking.Entity.Booking;
import fi.vamk.e1800957.foodbooking.Entity.Item;
import fi.vamk.e1800957.foodbooking.Repository.BookingRepository;
import fi.vamk.e1800957.foodbooking.Repository.ItemRepository;

@SpringBootApplication
public class FoodBookingApplication {
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private BookingRepository bookingRepository;

	public static void main(String[] args) {
		SpringApplication.run(FoodBookingApplication.class, args);
	}
	
	@Bean
	public void init() {
		
		
		Item i1= new Item("Pho", "Pho is essentially Vietnam’s signature dish, comprising rice noodles in a flavourful soup with meat and various greens, plus a side of nuoc cham (fermented fish) or chilli sauce",  10, "Available", "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/0/image.jpg");
		Item i2= new Item("Banh Mi", "Banh mi is a unique French-Vietnamese sandwich that’s great for when you’re in need of a quick meal",15, "Available", "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/00/image.jpg");
		Item i3= new Item("Banh Xeo (Crispy Pancake)", "Similar to a crepe or pancake, banh xeo is made of rice flour, coconut milk, and turmeric, which you can fill it with vermicelli noodles, chicken, pork or beef slices, shrimps, sliced onions, beansprouts, and mushrooms",10, "Available", "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/01/image.jpg");
		Item i4= new Item("Goi Cuon (Vietnamese Fresh Spring Rolls)", "Goi cuon (Vietnamese fresh spring rolls) consist of thin vermicelli noodles, pork slices, shrimp, basil, and lettuce, all tightly wrapped in translucent banh trang (rice papers)",5,  "Available", "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/02/image.jpg");
		Item i5= new Item("Mi Quang (Vietnamese Turmeric Noodles)", "Mi quang may be available at most restaurants in Vietnam, but it actually originates from Da Nang", 20,  "Available", "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/03/image.jpg");
		Item i6= new Item("Bun Thit Nuong (Vermicelli Noodles With Grilled Pork)", "Bun thit nuong comprises thin vermicelli rice noodles, chopped lettuce, sliced cucumber, bean sprouts, pickled daikon, basil, chopped peanuts, and mint, topped with grilled pork shoulder",20,  "Available", "http://static.asiawebdirect.com/m/.imaging/712x474/website/bangkok/portals/vietnam/homepage/vietnam-top10s/best-vietnamese-food/allParagraphs/00/top10Set/04/image.jpg");
		
		
		Booking b1= new Booking("Huiop", "fvcvcxvx", "cvxvcxv", "cvxvxvxv");
		Booking b2= new Booking("Huy", "fddsfg", "fgdgfd", "gfdgd");
		Booking b3= new Booking("Huycvxvcx", "fddsfg", "fgdgfd", "gfdgd");
		
		List<Item> list1= new ArrayList<Item>();
		list1.add(i1);
		list1.add(i2);
		
		
		List<Item> list2= new ArrayList<Item>();
		list2.add(i3);
		list2.add(i4);
		
		List<Item> list3= new ArrayList<Item>();
		list3.add(i5);
		list3.add(i6);
		
		
		b1.setItems(list1);
		b2.setItems(list2);
		b3.setItems(list3);

		
		itemRepository.save(i1);
		itemRepository.save(i2);
		itemRepository.save(i3);
		itemRepository.save(i4);
		itemRepository.save(i5);
		itemRepository.save(i6);
		
		
		double price1=0;
		for (Item item : list1) {
			price1+= item.getPrice();
		}
		
		double price2=0;
		for (Item item : list2) {
			price2+= item.getPrice();
		}
		
		double price3=0;
		for (Item item : list3) {
			price3+= item.getPrice();
		}
		
		
		b1.setPrice(price1);
		b2.setPrice(price2);
		b3.setPrice(price3);
		
		bookingRepository.save(b1);
		bookingRepository.save(b2);
		bookingRepository.save(b3);


	}

}
