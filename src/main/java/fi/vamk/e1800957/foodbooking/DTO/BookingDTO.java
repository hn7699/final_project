package fi.vamk.e1800957.foodbooking.DTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fi.vamk.e1800957.foodbooking.Entity.Booking;
import fi.vamk.e1800957.foodbooking.Entity.Item;

public class BookingDTO {
	private int id;
	private String customer_name;
	private String customer_address;
	private String customer_phone;
	private String status;
	private double price;
	private String time;
	private List<ItemDTO> items;
	
	public BookingDTO() {
		super();
	}
	public BookingDTO(String customer_name, String customer_address, String customer_phone, String status,
			List<ItemDTO> items) {
		super();
		this.customer_name = customer_name;
		this.customer_address = customer_address;
		this.customer_phone = customer_phone;
		this.status = status;
		this.items=items;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getCustomer_address() {
		return customer_address;
	}
	public void setCustomer_address(String customer_address) {
		this.customer_address = customer_address;
	}
	public String getCustomer_phone() {
		return customer_phone;
	}
	public void setCustomer_phone(String customer_phone) {
		this.customer_phone = customer_phone;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

	public List<ItemDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemDTO> items) {
		this.items = items;
	}
	public double getPrice() {
		return price;
	}
	
	
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Booking convert() {
		try {
			Booking b1= new Booking();
			b1.setCustomer_name(getCustomer_name());
			b1.setCustomer_address(getCustomer_address());
			b1.setCustomer_phone(getCustomer_phone());
			b1.setStatus(getStatus());
		    b1.setTime(new Date());
			return b1;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	@Override
	public String toString() {
		return "BookingDTO [id=" + id + ", customer_name=" + customer_name + ", customer_address=" + customer_address
				+ ", customer_phone=" + customer_phone + ", status=" + status + ", price=" + price + ", time=" + time
				+ ", items=" + items + "]";
	}
	
	
	
}
