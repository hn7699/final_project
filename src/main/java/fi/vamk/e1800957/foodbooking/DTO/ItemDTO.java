package fi.vamk.e1800957.foodbooking.DTO;

import fi.vamk.e1800957.foodbooking.Entity.Item;

public class ItemDTO {
	private int id;
	private String title;
	private String description;
	private String status;
	private double price;
	private String image;
	
	
	public ItemDTO() {
		super();
	}


	public ItemDTO(String title, String description, String status, double price, String image) {
		super();
		this.title = title;
		this.description = description;
		this.price = price;
		this.status=status;
		this.image=image;
	}


	public ItemDTO(int id, String title, String description, String status, double price, String image) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.price = price;
		this.status=status;
		this.image=image;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}
	
	


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public Item convert() {
		Item i= new Item();
		i.setId(getId());
		i.setTitle(this.title);
		i.setDescription(this.description);
		i.setPrice(this.price);
		i.setStatus(this.status);
		i.setImage(this.image);
		return i;
	}


	@Override
	public String toString() {
		return "ItemDTO [id=" + id + ", title=" + title + ", description=" + description + ", price=" + price + "]";
	}
	
	
	

}
