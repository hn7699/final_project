<h1>Online Booking Product System</h1>
<h3>Hoang Nguyen</h3>
<h3>e1800957</h3>


<h1>Analysis</h1>

<h2>Case description</h2> 


<ul>

<li>Actors and their roles</li>

| Actors | Roles |
| ------ | ------ |
| Client | Person purchasing products (food, drink,.) online |
| Manager | Person responsible for the system | 
| Supplier | The person or organization selling their products through the online shopping cart system. | 
| Bank | The supplier’s bank. The bank get involved to debit the client’s account and credit the supplier’s account during a purchase | 
| System | Dummy actor representing the system. This actor is a dummy actor used to represent the system in interaction diagrams for the use cases | 
| Visitor | Person who is only viewing product without purchasing | 

<li>Tasks the actors would like to do</li>


| Tasks | Description |
| ------ | ------ |
| Manage shopping cart - Manager | This task describes how a manager can manage user’s account or control the systems. More detail, managers should have following rights: Show table of products, Show table of bookings, Add items, Add bookings, Manage client’s accounts, Search for item, Edit items,Delete bookings, Search for bookings, Edit bookings|
| Mange client’s account - clients | This task describes how the clients manage their bookings. Concretely, clients should have rights:Change their information, Check out, Add bookings, Search for their interesting products, Give complaints | 
| Check out - Bank | This task describes how the client finalizes his/her purchases by checking out from the shopping cart. Payment is processed in this task | 
| Create a client account - visitors | This task describes how client can easily create her/his account (even this is the first time he/she access website) | 
| Tasks which are responsible by suppliers | This task describes how the supplier take their responsibilities to make sure that clients are satisfied with their services. Concretely, suppliers should have the following rights: Recall products, Ship orders, Handle complaints, Advertise products  | 
 
<li>Data input</li>

* items object Json
```
{
  "description": "string",
  "image": "string",
  "price": 0,
  "status": "string",
  "title": "string"
}
```

* booking objects Json

```
{
  "customer_address": "string",
  "customer_name": "string",
  "customer_phone": "string",
  "items": [
    {
      "description": "string",
      "id": 0,
      "image": "string",
      "price": 0,
      "status": "string",
      "title": "string"
    }
  ],
  "status": "string"
}
```
<li>Data output</li>

* HTTP response status.

<h3>What kind of algorithms there are</h3>
In this project, I will use some basic algorithms to handle requests:

* Sorting: use for sort orders
* Searching: use for search products

<h2>Table of functional requirements</h2>

| ID | Description | Priority (1 = must, 2 = should, 3= nice to have) |
| ------ | ------ |------ |
| 1 | Show a list of products in nice way | 1 | 
| 2 | Show a table of booking | 1 |
| 3 | Edit booking | 1 | 
| 4 | Edit items | 1 |
| 5 | Search items | 1 |
| 6 | Search bookings | 1 |
| 7 | Add items | 1 |
| 8 | Add bookings | 1 |
| 9 | Delete items | 1 |
| 10 | Delete bookings | 1 |
| 11 | Ship orders | 2 |
| 12 | Log in | 2 |
| 13 | File Complaint | 2 |
| 14 | Mange Client Account | 2 |
| 15 | Mage Product Catalog | 2 |
| 16 | Recall Product | 2 |
| 17 | Advertise Product | 3 |
| 18 | Mange Supplier’s bank | 3 |
<h2>Project schedule</h2>
![my image](Pics/schedule.png?raw=true "Use case")
<h2>Project plan document (including the schedule) using the template</h2>

<h3>General Description of the Project</h3>

*  This project is final project on “Software Engineering method” course. This project contains the model for the Online Booking Product system (food, clothes,..) , that allows suppliers to sell their products directly online to customers.

<h3>The Aim of the Project</h3>

*  The final product should be a web application that has user-friendly interface where users can easily navigate and use all the functions, including booking and viewing the products, as well as giving more specific descriptions and searching for a certain product. Additionally, the status of product (available or not,..) should be shown in real time.

<h3>Project team </h3>

| Name | Role | Email |
| ------ | ------ |------ |
| Hoang Nguyen | Manager/ developer | e1800957@edu.vamk.fi | 


<h3>The tools and devices needed for the project are:</h3>

* Visual Paradigm 14.0: for drawing user cases and diagrams
* MS Visual Studio 2015: for coding and testing
* MS Project 2016: for planning the schedule and resource allocation
* VAMK Git: for version control
* ECLIPSE : For coding back-end side

<h3>Documentation Methods and Tools:</h3>

* Programming Languages: Java, Javascript.
* Libraries: Spring, React
* Control system: Git
* Documentation: Project plan, project schedule, use case and sequence diagram,...

<h3>Working Methods</h3>

*  The project proceeds according to the Scrum framework. 

<h3>Risk Managements</h3>

| Risk | Analysis | Action |
| ------ | ------ |------ |
| Major changes of requirements during project | 2*3=6 | Write down, prioritize and discuss with customer regularly | 
| Do not make project plan | 2*3=6 | Create good project plan. |
| Do not have enough knowledge for doing task | 2*2=4 | Find out resources to learn more | 
| Ambiguity in functional requirements | 2*2=4 | Review and discuss carefully  then adjust if needed |
| Test cases do not cover enough regular situations | 2*2=4 | Make more test. |
| Personal business | 1*1=1 | Rearrange time allocation |
  
<h3>Work Roles and Areas of Responsibility</h3>

| Name | Role | Responsibility |
| ------ | ------ |------ |
| Hoang Nguyen | Manager | Full-stack developer, documentations | 

<h3>Reference List</h3>

*  Materials from “Software Engineering method” course.


<h3>Project Control Plan</h3> 

*  The tasks are marked as “high priority” should be completed first. Every single step need to be tested before releasing, the test plan should be written in clear ways. 





<h3>Use case diagram drawn with Visual Paradigm</h2>
![my image](Pics/use_case.png?raw=true "Use case")
<h2>Test plan</h2>

| Test | Status |
| ------ | ------ |
| the application should save, fetch by id and delete the item | accepted |
| the application should save, fetch by id and delete the booking | accepted | 
| HTTP GET test: get list of bookings and items | accepted | 
| HTTP GET test: get method by id | accepted | 
| HTTP POST test: post method  | accepted | 
| HTTP PUT test: put method | accepted | 
| HTTP DELETE test: delete method | accepted | 

<h1>Design</h1>

<h2>Sequence diagrams (one from client side one from server side)  drawn with Visual Paradigm</h2>
![my image](Pics/sequence.png?raw=true "Sequence")
<h2>Package diagram drawn with Visual Paradigm</h2>
![my image](Pics/package.png?raw=true "Package")
<h2>Class diagrams (one from client side one from server side)  drawn with Visual Paradigm</h2>
![my image](Pics/class.png?raw=true "Class")
<h2>Two user stories</h2>
Like "As a Finnish citizen I want to know the actual Corona numbers on daily based listing the day, daily new sickness and daily died. I would like to see 30 rows per time and able to scroll next 30."

<h1>Implementation</h1>

<h2>Java Spring Boot back end having minimum two tables</h2>

* Booking table
* Item table

<h2>React front end having minimum two tables fetching the database data</h2>

* Booking table
* Item table
* Item by booking table

<h1>Testing</h1>

<h2>Spring Boot test cases</h2>
<ul>
<li>Add item to database
<li>Get one from database based on id
<li>Get all from database and compare amount
<li>Test RESTful API GET one
<li>Test RESTful API POST 
</ul>
<h2>React Test cases</h2>
Test that the table (or tables) exist

<h1>Documentation</h1>
<ul>
<li>Swagger documentation for RESTful API</li>
<li>README.md documentation for Spring Boot backend</li>
<li>README.md documentation for React front end (different gitlab -project)</li>
</ul>
<h1>Deployment</h1>
<ul>
<li>Back end deployed to Heroku in https://booking-product.herokuapp.com/swagger-ui.html  (username: user, password: password123)</li>
<li>Front end either  https://e1800957-foodbooking.herokuapp.com </li>
</ul>
